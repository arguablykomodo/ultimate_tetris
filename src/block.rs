use crate::coord::Coord;
use glium::*;

#[derive(Copy, Clone)]
pub struct Block {
  pub position: Coord,
  pub color: (f32, f32, f32),
}
implement_vertex!(Block, position, color);
