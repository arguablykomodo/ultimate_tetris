use crate::texture::load_texture;
use glium::{framebuffer::*, texture::srgb_texture2d::*, uniforms::MagnifySamplerFilter::*, *};

#[derive(Copy, Clone)]
pub struct Vertex {
  position: [f32; 2],
  uv: [f32; 2],
}
implement_vertex!(Vertex, position, uv);

pub struct Effects<'a> {
  frame_a: SrgbTexture2d,
  frame_b: SrgbTexture2d,
  switch: bool,

  background: SrgbTexture2d,
  game_over: SrgbTexture2d,
  display: &'a Display,
  vertex_buffer: VertexBuffer<Vertex>,
  index_buffer: IndexBuffer<u16>,
  program: Program,
}

impl<'a> Effects<'a> {
  pub fn new(display: &Display) -> Effects {
    Effects {
      frame_a: SrgbTexture2d::empty(display, 178, 170).unwrap(),
      frame_b: SrgbTexture2d::empty(display, 178, 170).unwrap(),
      switch: false,

      background: load_texture(include_bytes!("background.png"), display),
      game_over: load_texture(include_bytes!("game_over.png"), display),
      display,
      vertex_buffer: VertexBuffer::new(
        display,
        &[
          Vertex {
            position: [-1.0, -1.0],
            uv: [0.0, 0.0],
          },
          Vertex {
            position: [-1.0, 1.0],
            uv: [0.0, 1.0],
          },
          Vertex {
            position: [1.0, 1.0],
            uv: [1.0, 1.0],
          },
          Vertex {
            position: [1.0, -1.0],
            uv: [1.0, 0.0],
          },
        ],
      )
      .unwrap(),
      index_buffer: IndexBuffer::new(
        display,
        index::PrimitiveType::TriangleStrip,
        &[1 as u16, 2, 0, 3],
      )
      .unwrap(),
      program: Program::from_source(
        display,
        &include_str!("effects.vert"),
        &concat!(
          "#version 330 core",
          include_str!("../../noise/src/noise3D.glsl"),
          include_str!("effects.frag")
        ),
        None,
      )
      .unwrap(),
    }
  }

  pub fn frame(&self, last: bool) -> &SrgbTexture2d {
    if self.switch ^ last {
      &self.frame_a
    } else {
      &self.frame_b
    }
  }

  pub fn surface(&mut self, last: bool) -> SimpleFrameBuffer {
    if self.switch ^ last {
      SimpleFrameBuffer::new(self.display, &self.frame_a).unwrap()
    } else {
      SimpleFrameBuffer::new(self.display, &self.frame_b).unwrap()
    }
  }

  pub fn draw(&mut self, target: &mut impl Surface, t: f32, lost: bool) {
    target
      .draw(
        &self.vertex_buffer,
        &self.index_buffer,
        &self.program,
        &uniform! {
          background: self.background.sampled().magnify_filter(Nearest),
          game_over: self.game_over.sampled().magnify_filter(Nearest),
          frame: self.frame(false).sampled().magnify_filter(Nearest),
          last_frame: self.frame(true).sampled().magnify_filter(Nearest),
          t: t,
          lost: lost,
        },
        &Default::default(),
      )
      .unwrap();

    target.fill(&self.surface(false), Nearest);

    self.surface(true).clear_color(0.0, 0.0, 0.0, 0.0);
    self.switch = !self.switch;
  }
}
