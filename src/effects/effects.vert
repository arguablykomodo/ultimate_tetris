#version 330 core

in vec2 position;
in vec2 uv;

out vec2 _uv;

void main() {
  _uv = uv;
  gl_Position = vec4(position, 0.0, 1.0);
}
