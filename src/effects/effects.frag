uniform sampler2D background;
uniform sampler2D frame;
uniform sampler2D last_frame;
uniform sampler2D game_over;
uniform float t;
uniform bool lost;

in vec2 _uv;
out vec4 color;

const vec4 GAMMA_CORRECTION = vec4(2.2);
const vec2 TEXURE_SIZE = vec2(178, 170);

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
  vec2 uv = (floor(_uv * TEXURE_SIZE) + 0.5) / TEXURE_SIZE;
  vec4 background = texture(background, uv) + snoise(vec3(uv * TEXURE_SIZE, t / 2)) * vec4(0.0, 0.01, 0.02, 1.0);
  vec4 current = texture(frame, uv);
  vec4 shadow = //pow(
    mix(
      vec4(0),
      texture(last_frame, uv),
      min(rand(uv + t) * 5, 1)
    )/*,
    GAMMA_CORRECTION
  )*/;
  color = max(background, mix(shadow, current, current.a));

  if (lost) {
    vec4 end_screen = texture(game_over, uv);
    color = vec4(mix(color.rgb, end_screen.rgb, end_screen.a), 1.0);
  }
}
