use crate::block::Block;
use crate::coord::Coord;
use glium::*;

#[derive(Copy, Clone)]
pub enum TetrominoShape {
  I,
  O,
  T,
  S,
  Z,
  J,
  L,
}

impl TetrominoShape {
  pub fn to_string(self) -> &'static str {
    match self {
      TetrominoShape::I => "i",
      TetrominoShape::O => "o",
      TetrominoShape::T => "t",
      TetrominoShape::S => "s",
      TetrominoShape::Z => "z",
      TetrominoShape::J => "j",
      TetrominoShape::L => "l",
    }
  }
}

pub struct Tetromino {
  pub shape: TetrominoShape,
  pub blocks: [Coord; 4],
  pub in_field: bool,
  center: (f32, f32),
  pub rotation: i32,
  f_rotation: f32,
  pub position: Coord,
  f_position: (f32, f32),
  pub shadow: i32,
  f_shadow: f32,

  vertex_buffer: VertexBuffer<Block>,
  index_buffer: index::NoIndices,
  program: Program,
}

impl Tetromino {
  pub fn new(shape: TetrominoShape, position: Coord, display: &Display) -> Tetromino {
    let blocks = include!("shapes.rs");
    let color = include!("colors.rs");
    let mut vertices = Vec::new();
    for block in blocks.iter() {
      vertices.push(Block {
        position: *block,
        color,
      });
    }

    Tetromino {
      shape,
      blocks,
      in_field: false,
      center: include!("centers.rs"),
      rotation: 0,
      f_rotation: 0.0,
      position,
      f_position: (position.0 as f32, position.1 as f32),
      shadow: 0,
      f_shadow: position.1 as f32,

      vertex_buffer: VertexBuffer::new(display, &vertices).unwrap(),
      index_buffer: index::NoIndices(index::PrimitiveType::LinesListAdjacency),
      program: Program::from_source(
        display,
        include_str!("tetromino.vert"),
        include_str!("tetromino.frag"),
        Some(concat!(
          "#version 330 core\n",
          include_str!("../../noise/src/noise2D.glsl"),
          include_str!("tetromino.geom")
        )),
      )
      .unwrap(),
    }
  }

  pub fn transform(&self) -> Vec<Coord> {
    let mut transformed_blocks = Vec::new();
    for b in self.blocks.iter() {
      let b = *b - self.center;
      let b = match self.rotation % 4 {
        0 => b,
        1 => (b.1, -b.0 - 1 as f32),
        2 => (-b.0 - 1 as f32, -b.1 - 1 as f32),
        3 => (-b.1 - 1 as f32, b.0),
        _ => b,
      };
      let b = Coord((b.0 + self.center.0) as i32, (b.1 + self.center.1) as i32);
      let b = b + self.position;
      transformed_blocks.push(b);
    }
    return transformed_blocks;
  }

  pub fn draw(&mut self, target: &mut framebuffer::SimpleFrameBuffer, t: f32) {
    self.f_shadow = self.f_shadow + 0.4 * (self.shadow as f32 - self.f_shadow);
    self.f_rotation = self.f_rotation + 0.4 * (self.rotation as f32 - self.f_rotation);
    self.f_position = (
      self.f_position.0 + 0.4 * (self.position.0 as f32 - self.f_position.0),
      self.f_position.1 + 0.4 * (self.position.1 as f32 - self.f_position.1),
    );

    target
      .draw(
        &self.vertex_buffer,
        &self.index_buffer,
        &self.program,
        &uniform! {
          in_field: self.in_field,
          position: if self.in_field { self.f_position } else { match self.shape {
            TetrominoShape::I => (10.375 + 0.5, 12.0 + 1.0),
            TetrominoShape::O => (10.375 + 1.5, 12.0 + 0.5),
            _ => (10.375 + 1.0, 12.0 + 0.5)
          } },
          rotation: self.f_rotation,
          center: self.center,
          shadow: self.f_shadow,
          t: t
        },
        &Default::default(),
      )
      .unwrap();
  }
}
