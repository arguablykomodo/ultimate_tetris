match shape {
  TetrominoShape::O => (0.5, 0.5),
  _ => (1.5, 0.5),
}