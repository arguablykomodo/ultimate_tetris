match shape {
  TetrominoShape::I => (0.0, 1.0, 1.0),
  TetrominoShape::O => (1.0, 1.0, 0.0),
  TetrominoShape::T => (0.5, 0.0, 0.5),
  TetrominoShape::S => (0.0, 1.0, 0.0),
  TetrominoShape::Z => (1.0, 0.0, 0.0),
  TetrominoShape::J => (0.0, 0.0, 1.0),
  TetrominoShape::L => (1.0, 0.5, 0.0),
}