match shape {
  TetrominoShape::I => [Coord(0, 0), Coord(1, 0), Coord(2, 0), Coord(3, 0)],
  TetrominoShape::O => [Coord(0, 0), Coord(1, 0), Coord(0, 1), Coord(1, 1)],
  TetrominoShape::T => [Coord(0, 0), Coord(1, 0), Coord(2, 0), Coord(1, 1)],
  TetrominoShape::S => [Coord(0, 0), Coord(1, 0), Coord(1, 1), Coord(2, 1)],
  TetrominoShape::Z => [Coord(1, 0), Coord(2, 0), Coord(0, 1), Coord(1, 1)],
  TetrominoShape::J => [Coord(0, 0), Coord(1, 0), Coord(2, 0), Coord(0, 1)],
  TetrominoShape::L => [Coord(0, 0), Coord(1, 0), Coord(2, 0), Coord(2, 1)],
}