#define PI 3.1415926538

uniform bool in_field;
uniform vec2 position;
uniform vec2 center;
uniform float rotation;
uniform float shadow;
uniform float t;

const vec2 pixel_size = 1 / vec2(178, 170);
const vec2 block_size = pixel_size * vec2(8, 8);
const vec2 pixel_offset = pixel_size * vec2(5, 5);

// We use lines_adjacency because we get 4 vertices, that way we can make sure the shadows are under the tetromino
layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 32) out;

in vec3 _color[];
out vec3 color;

void vertex(vec2 pos, vec2 offset, vec2 coords, mat2 matrix) {
  pos += offset;
  pos -= center;
  pos *= matrix;
  pos += center;
  pos += coords;
  gl_Position = vec4((pos * block_size + pixel_offset) * 2 - 1, 0, 1);
  EmitVertex();
}

void generate_block(vec2 pos, vec2 coords, mat2 matrix) {
  vertex(pos, vec2(0, 0), coords, matrix);
  vertex(pos, vec2(0, 1), coords, matrix);
  vertex(pos, vec2(1, 0), coords, matrix);
  vertex(pos, vec2(1, 1), coords, matrix);
  EndPrimitive();
}

mat2 to_matrix(float rot) {
  float rads = -rot * PI / 2;
  float s = sin(rads);
  float c = cos(rads);
  return mat2(c, -s, s, c);
}

void main() {
  if (in_field) {
    mat2 matrix = to_matrix(rotation);
    color = mix(_color[0], vec3(0), 0.8);
    generate_block(gl_in[0].gl_Position.xy, vec2(position.x, shadow), matrix);
    generate_block(gl_in[1].gl_Position.xy, vec2(position.x, shadow), matrix);
    generate_block(gl_in[2].gl_Position.xy, vec2(position.x, shadow), matrix);
    generate_block(gl_in[3].gl_Position.xy, vec2(position.x, shadow), matrix);
  }

  mat2 matrix = to_matrix(rotation + snoise(vec2((int(in_field) * 2 - 1) * t * 0.5)) * .2);
  color = _color[0];
  generate_block(gl_in[0].gl_Position.xy, position, matrix);
  generate_block(gl_in[1].gl_Position.xy, position, matrix);
  generate_block(gl_in[2].gl_Position.xy, position, matrix);
  generate_block(gl_in[3].gl_Position.xy, position, matrix);
}
