use crate::coord::Coord;
use crate::effects::Effects;
use crate::row::Row;
use crate::tetromino::{TetrominoShape::*, *};
use crate::text::Text;
use discord_rpc_client::Client;
use glium::*;
use std::time::*;

pub struct Game<'a> {
  rows: Vec<Row<'a>>,
  tetromino: Option<Tetromino>,
  next_tetromino: Option<Tetromino>,
  bag: Vec<TetrominoShape>,
  text: Vec<Text<'a>>,

  score: i32,
  level: i32,
  interval: Duration,
  lines_cleared: i32,
  last_frame: Instant,
  time: Instant,

  drpc: Client,
  timestamp: u64,

  display: &'a Display,
  effects: Effects<'a>,
}

impl<'a> Game<'a> {
  pub fn new(display: &'a Display, font: &'a texture::SrgbTexture2d) -> Game<'a> {
    let mut drpc = Client::new(530173279898435594);
    drpc.start();

    let mut game = Game {
      rows: Vec::new(),
      tetromino: None,
      next_tetromino: None,
      bag: vec![I, O, T, S, Z, J, L],
      text: vec![
        Text::new("Score: 0", Coord(0, 0), display, font),
        Text::new("Level: 1", Coord(0, 1), display, font),
      ],

      score: 0,
      level: 1,
      lines_cleared: 0,
      interval: Duration::from_secs(1),
      last_frame: Instant::now(),
      time: Instant::now(),

      drpc,
      timestamp: SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs(),

      display,
      effects: Effects::new(display),
    };

    for y in 0..20 {
      game.rows.push(Row::new(y, display));
    }

    use rand::Rng;
    let i = rand::thread_rng().gen_range(0, game.bag.len());
    let shape = game.bag.remove(i);
    game.next_tetromino = Some(Tetromino::new(shape, Coord(5, 20), game.display));

    game.new_tetromino();
    game.update_rpc();

    return game;
  }

  pub fn reset(&mut self) {
    self.rows = Vec::new();
    self.tetromino = None;
    self.next_tetromino = None;
    self.bag = vec![I, O, T, S, Z, J, L];
    self.text[0].set_text("Score: 0");
    self.text[1].set_text("Level: 1");
    self.score = 0;
    self.level = 1;
    self.lines_cleared = 0;
    self.interval = Duration::from_secs(1);
    self.last_frame = Instant::now();
    self.time = self.last_frame;

    for y in 0..20 {
      self.rows.push(Row::new(y, self.display));
    }

    use rand::Rng;
    let i = rand::thread_rng().gen_range(0, self.bag.len());
    let shape = self.bag.remove(i);
    self.next_tetromino = Some(Tetromino::new(shape, Coord(5, 20), self.display));

    self.new_tetromino();
    self.update_rpc();
  }

  fn update_rpc(&mut self) {
    if let (Some(t), Some(next_t)) = (&self.tetromino, &self.next_tetromino) {
      use std::thread;

      let state = format!("Level: {}", &self.level);
      let details = format!("Score: {}", &self.score);
      let large_image = t.shape.to_string();
      let small_image = next_t.shape.to_string();
      let timestamp = self.timestamp;

      // There has to be a better way to do this
      let mut rpc = self.drpc.clone();
      thread::spawn(move || {
        rpc
          .set_activity(|a| {
            a.state(state)
              .details(details)
              .timestamps(|t| t.start(timestamp))
              .assets(|ass| ass.large_image(large_image).small_image(small_image))
          })
          .unwrap();
      });
    }
  }

  fn collides(t: &Tetromino, rows: &Vec<Row>) -> bool {
    t.transform().iter().any(|&b| {
      b.0 < 0 || b.0 >= 10 || b.1 < 0 || b.1 >= 20 || rows[b.1 as usize].blocks[b.0 as usize]
    })
  }

  fn new_tetromino(&mut self) {
    use rand::Rng;
    use std::mem::replace;

    if self.bag.is_empty() {
      self.bag = vec![I, O, T, S, Z, J, L];
    }
    let i = rand::thread_rng().gen_range(0, self.bag.len());
    let shape = self.bag.remove(i);
    let mut tetromino = replace(
      &mut self.next_tetromino,
      Some(Tetromino::new(shape, Coord(12, 12), self.display)),
    )
    .unwrap();

    tetromino.position = Coord(
      match tetromino.shape {
        O => 4,
        _ => 3,
      },
      20,
    );
    tetromino.in_field = true;
    let mut blocks = tetromino.blocks.iter();
    while blocks.any(|&b| b.1 + tetromino.position.1 >= 20) {
      tetromino.position.1 -= 1;
    }
    if Game::collides(&tetromino, &self.rows) {
      self.tetromino = None;
    } else {
      self.tetromino = Some(tetromino);
      self.update_shadow();
    }
  }

  fn update_shadow(&mut self) {
    if let Some(t) = &mut self.tetromino {
      let old_y = t.position.1;
      while !Game::collides(t, &self.rows) {
        t.position.1 -= 1;
      }
      t.shadow = t.position.1 + 1;
      t.position.1 = old_y;
    }
  }

  pub fn move_tetromino(&mut self, direction: i32) {
    if let Some(t) = &mut self.tetromino {
      t.position.0 += direction;
      if Game::collides(t, &self.rows) {
        t.position.0 -= direction;
      }
      self.update_shadow();
    }
  }

  pub fn soft_drop(&mut self) {
    if let Some(t) = &mut self.tetromino {
      self.last_frame = Instant::now();
      t.position.1 -= 1;
      if Game::collides(t, &self.rows) {
        t.position.1 += 1;
        self.place_tetromino();
      }
      self.update_shadow();
    }
  }

  pub fn hard_drop(&mut self) {
    if let Some(t) = &mut self.tetromino {
      while !Game::collides(t, &self.rows) {
        t.position.1 -= 1;
      }
      t.position.1 += 1;
      self.place_tetromino();
    }
  }

  pub fn rotate(&mut self) {
    if let Some(t) = &mut self.tetromino {
      let kick_data = include!("kick_data.rs");
      let old_r = t.rotation as usize;
      let new_r = old_r + 1;
      t.rotation = new_r as i32;
      for i in 0..kick_data[new_r % 4].len() {
        let offset = kick_data[old_r % 4][i] - kick_data[new_r % 4][i];
        t.position += offset;
        if !Game::collides(t, &self.rows) {
          self.update_shadow();
          return;
        }
        t.position -= offset;
      }
      t.rotation = old_r as i32;
    }
  }

  fn place_tetromino(&mut self) {
    use crate::block::Block;
    use std::collections::HashMap;
    if let Some(t) = &mut self.tetromino {
      let shape = t.shape;
      let color = include!("../tetromino/colors.rs");
      let mut rows: HashMap<&i32, Vec<Block>> = HashMap::with_capacity(4);
      let mut to_clear = Vec::new();
      let blocks = t.transform();
      for block in blocks.iter() {
        match rows.get_mut(&block.1) {
          Some(r) => r.push(Block {
            position: *block,
            color,
          }),
          None => {
            rows.insert(
              &block.1,
              vec![Block {
                position: *block,
                color,
              }],
            );
          }
        }
      }
      for (y, blocks) in rows.iter_mut() {
        self.rows[**y as usize].set_blocks(blocks);
        if self.rows[**y as usize].blocks.iter().all(|&b| b) {
          to_clear.push(**y as usize);
        }
      }
      to_clear.sort();
      self.clear(to_clear);
      self.new_tetromino();
      self.update_rpc();
    }
  }

  fn clear(&mut self, rows: Vec<usize>) {
    if rows.len() == 0 {
      return;
    }

    for y in rows.iter().rev() {
      self.rows.remove(*y);
      self.rows.push(Row::new(19, self.display));
    }

    for i in 0..20 {
      self.rows[i].y = i as i32;
      self.rows[i].rumble = rows.len() as f32 / 4.0;
    }

    self.score += match rows.len() {
      1 => 100 * self.level,
      2 => 300 * self.level,
      3 => 500 * self.level,
      4 => 800 * self.level,
      _ => 0,
    };
    self.lines_cleared += rows.len() as i32;
    if self.lines_cleared >= 10 {
      self.level += 1;
      self.lines_cleared -= 10;
      self.interval = Duration::from_millis(
        ((0.8 - (0.007 * (self.level as f32 - 1.0))).powf(self.level as f32 - 1.0) * 1000.0) as u64,
      );
      self.text[1].set_text(format!("Level: {}", self.level).as_str());
    }

    self.text[0].set_text(format!("Score: {}", self.score).as_str());
  }

  pub fn draw(&mut self, target: &mut impl Surface) {
    let elapsed = self.time.elapsed();
    let time = elapsed.subsec_millis() as f32 / 1000.0 + elapsed.as_secs() as f32;

    if self.last_frame.elapsed() > self.interval {
      self.soft_drop();
    }

    let mut frame = self.effects.surface(false);

    if let Some(t) = &mut self.tetromino {
      t.draw(&mut frame, time);
    }
    if let Some(t) = &mut self.next_tetromino {
      t.draw(&mut frame, time);
    }
    for row in self.rows.iter_mut() {
      row.draw(&mut frame, time);
    }
    for text in self.text.iter() {
      text.draw(&mut frame);
    }

    self.effects.draw(target, time, self.tetromino.is_none());
  }
}
