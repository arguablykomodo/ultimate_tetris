match t.shape {
  TetrominoShape::O => vec![
    vec![Coord(0, 0)],
    vec![Coord(0, -1)],
    vec![Coord(-1, -1)],
    vec![Coord(-1, 0)],
  ],
  TetrominoShape::I => vec![
    vec![Coord(0, 0), Coord(-1, 0), Coord(2, 0), Coord(-1, 0), Coord(2, 0)],
    vec![Coord(-1, 0), Coord(0, 0), Coord(0, 0), Coord(0, 1), Coord(0, -2)],
    vec![Coord(-1, 1), Coord(1, 1), Coord(-2, 1), Coord(1, 0), Coord(-2, 0)],
    vec![Coord(0, 1), Coord(0, 1), Coord(0, 1), Coord(0, -1), Coord(0, 2)],
  ],
  _ => vec![
    vec![Coord(0, 0), Coord(0, 0), Coord(0, 0), Coord(0, 0), Coord(0, 0)],
    vec![Coord(0, 0), Coord(1, 0), Coord(1, -1), Coord(0, 2), Coord(1, 2)],
    vec![Coord(0, 0), Coord(0, 0), Coord(0, 0), Coord(0, 0), Coord(0, 0)],
    vec![Coord(0, 0), Coord(-1, 0), Coord(-1, -1), Coord(0, 2), Coord(-1, 2)],
  ],
}
