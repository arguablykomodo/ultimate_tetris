#[derive(Copy, Clone, Debug)]
pub struct Coord(pub i32, pub i32);

use std::ops::{Add, AddAssign, Sub, SubAssign};

impl Add for Coord {
  type Output = Coord;
  fn add(self, other: Coord) -> Coord {
    Coord(self.0 + other.0, self.1 + other.1)
  }
}

impl AddAssign for Coord {
  fn add_assign(&mut self, other: Coord) {
    *self = Coord(self.0 + other.0, self.1 + other.1);
  }
}

impl Sub for Coord {
  type Output = Coord;
  fn sub(self, other: Coord) -> Coord {
    Coord(self.0 - other.0, self.1 - other.1)
  }
}

impl Sub<(f32, f32)> for Coord {
  type Output = (f32, f32);
  fn sub(self, other: (f32, f32)) -> (f32, f32) {
    return (self.0 as f32 - other.0, self.1 as f32 - other.1);
  }
}

impl SubAssign for Coord {
  fn sub_assign(&mut self, other: Coord) {
    *self = Coord(self.0 - other.0, self.1 - other.1);
  }
}

use glium::uniforms::{AsUniformValue, UniformValue};
impl AsUniformValue for Coord {
  #[inline]
  fn as_uniform_value(&self) -> UniformValue {
    UniformValue::IntVec2([self.0, self.1])
  }
}

use glium::vertex::{Attribute, AttributeType};
unsafe impl Attribute for Coord {
  #[inline]
  fn get_type() -> AttributeType {
    AttributeType::I32I32
  }
}
