#version 330 core

in ivec2 position;
in ivec2 character;

out vec2 _character;

void main() {
  _character = vec2(character);
  gl_Position = vec4(
    position,
    0.0,
    1.0
  );
}
