#version 330 core

uniform ivec2 origin;

const vec2 texture_size = vec2(26, 3);
const vec2 pixel_size = 1 / vec2(178, 170);
const vec2 offset = pixel_size * vec2(92, 6);
const vec2 character_size = pixel_size * vec2(6, 7);

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec2 _character[];
out vec2 uv;

void vertex(vec2 pos, vec2 corner) {
  gl_Position = vec4(((pos + origin + corner) * character_size + offset) * 2 - 1, 0, 1);
  uv = (_character[0] + corner) / texture_size;
  EmitVertex();
}

void main() {
  vertex(gl_in[0].gl_Position.xy, vec2(0, 0));
  vertex(gl_in[0].gl_Position.xy, vec2(0, 1));
  vertex(gl_in[0].gl_Position.xy, vec2(1, 0));
  vertex(gl_in[0].gl_Position.xy, vec2(1, 1));
  EndPrimitive();
}
