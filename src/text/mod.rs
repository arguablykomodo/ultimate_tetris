use crate::coord::Coord;
use glium::{texture::*, *};

#[derive(Copy, Clone, Debug)]
struct Vertex {
  position: Coord,
  character: Coord,
}
implement_vertex!(Vertex, position, character);

pub struct Text<'a> {
  position: Coord,
  display: &'a Display,
  vertex_buffer: VertexBuffer<Vertex>,
  index_buffer: index::NoIndices,
  program: Program,
  font: &'a SrgbTexture2d,
  draw_params: DrawParameters<'a>,
}

impl<'a> Text<'a> {
  fn create_vertices(text: &str) -> Vec<Vertex> {
    let mut vertices = Vec::new();
    for (i, byte) in text.bytes().enumerate() {
      if byte == 32 {
        continue; // skip spaces
      }
      vertices.push(Vertex {
        position: Coord(i as i32, 0),
        character: match byte {
          48...57 => Coord(byte as i32 - 48, 2),  // numbers
          65...90 => Coord(byte as i32 - 65, 1),  // uppercase letters
          97...122 => Coord(byte as i32 - 97, 1), // lowercase letters
          33 => Coord(0, 0),                      // exclamation mark
          58 => Coord(1, 0),                      // colon
          _ => Coord(25, 0),
        },
      });
    }
    return vertices;
  }

  pub fn new(
    text: &str,
    position: Coord,
    display: &'a Display,
    font: &'a SrgbTexture2d,
  ) -> Text<'a> {
    Text {
      position,
      display,
      vertex_buffer: VertexBuffer::new(display, &Text::create_vertices(text)).unwrap(),
      index_buffer: index::NoIndices(index::PrimitiveType::Points),
      program: Program::from_source(
        display,
        include_str!("text.vert"),
        include_str!("text.frag"),
        Some(include_str!("text.geom")),
      )
      .unwrap(),
      font,
      draw_params: DrawParameters {
        blend: Blend::alpha_blending(),
        ..Default::default()
      },
    }
  }

  pub fn set_text(&mut self, text: &str) {
    self.vertex_buffer = VertexBuffer::new(self.display, &Text::create_vertices(text)).unwrap();
  }

  pub fn draw(&self, target: &mut framebuffer::SimpleFrameBuffer) {
    target
      .draw(
        &self.vertex_buffer,
        &self.index_buffer,
        &self.program,
        &uniform! {origin: self.position, font: self.font.sampled().magnify_filter(uniforms::MagnifySamplerFilter::Nearest)},
        &self.draw_params,
      )
      .unwrap();
  }
}
