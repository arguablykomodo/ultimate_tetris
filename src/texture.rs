use glium::{texture::*, *};
use image::*;
use std::io::Cursor;

pub fn load_texture(bytes: &[u8], display: &Display) -> SrgbTexture2d {
  let image = load(Cursor::new(bytes), PNG).unwrap().to_rgba();
  let image_dimensions = image.dimensions();
  let image = RawImage2d::from_raw_rgba_reversed(&image.into_raw(), image_dimensions);
  return SrgbTexture2d::new(display, image).unwrap();
}
