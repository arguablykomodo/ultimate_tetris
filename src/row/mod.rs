use crate::block::Block;
use glium::*;

pub struct Row<'a> {
  pub y: i32,
  f_y: f32,
  pub blocks: [bool; 10],
  pub rumble: f32,

  vertices: Vec<Block>,
  display: &'a Display,
  vertex_buffer: VertexBuffer<Block>,
  index_buffer: index::NoIndices,
  program: Program,
}

impl<'a> Row<'a> {
  pub fn new(y: i32, display: &'a Display) -> Row {
    Row {
      y,
      f_y: y as f32,
      blocks: [false; 10],
      rumble: 0.0,

      vertices: Vec::new(),
      display,
      vertex_buffer: VertexBuffer::new(display, &Vec::new()).unwrap(),
      index_buffer: index::NoIndices(index::PrimitiveType::Points),
      program: Program::from_source(
        display,
        include_str!("row.vert"),
        include_str!("row.frag"),
        Some(concat!(
          "#version 330 core\n",
          include_str!("../../noise/src/noise4D.glsl"),
          include_str!("row.geom")
        )),
      )
      .unwrap(),
    }
  }

  pub fn set_blocks(&mut self, blocks: &mut Vec<Block>) {
    for block in blocks.iter() {
      self.blocks[block.position.0 as usize] = true;
    }
    self.vertices.append(blocks);
    self.vertex_buffer = VertexBuffer::new(self.display, &self.vertices).unwrap();
  }

  pub fn draw(&mut self, target: &mut framebuffer::SimpleFrameBuffer, t: f32) {
    self.f_y = self.f_y + 0.4 * (self.y as f32 - self.f_y);
    self.rumble = self.rumble * 0.9;
    target
      .draw(
        &self.vertex_buffer,
        &self.index_buffer,
        &self.program,
        &uniform! {y: self.f_y, rumble: self.rumble, t: t},
        &Default::default(),
      )
      .unwrap();
  }
}
