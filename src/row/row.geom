uniform float y;
uniform float rumble;
uniform float t;

const vec2 pixel_size = 1 / vec2(178, 170);
const vec2 block_size = pixel_size * 8;
const vec2 pixel_offset = pixel_size * vec2(5, 5);

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec3 _color[];
out vec3 color;

void vertex(vec2 offset, vec2 rumble) {
  gl_Position = vec4(((vec2(gl_in[0].gl_Position.x, y) + offset + rumble) * block_size + pixel_offset) * 2 - 1, 0, 1);
  EmitVertex();
}

void main() {
  color = _color[0];
  vec2 rumble = rumble * vec2(snoise(vec4(color, t * 10)), snoise(vec4(t * 10, color)));
  vertex(vec2(0, 0), rumble);
  vertex(vec2(0, 1), rumble);
  vertex(vec2(1, 0), rumble);
  vertex(vec2(1, 1), rumble);
  EndPrimitive();
}
