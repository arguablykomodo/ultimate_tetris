mod block;
mod coord;
mod text;
mod texture;
use crate::texture::load_texture;
mod game;
mod row;
mod tetromino;
use crate::game::Game;
mod effects;
use glium::*;
use glium::glutin::*;
use std::time::*;

fn main() {
  let mut events_loop = EventsLoop::new();
  let display = {
    let window = WindowBuilder::new()
      .with_title("Ultimate Tetris")
      .with_dimensions(dpi::LogicalSize::new(178.0 * 3.0, 170.0 * 3.0));
    let context = ContextBuilder::new();
    Display::new(window, context, &events_loop).unwrap()
  };

  let font = load_texture(include_bytes!("text/font.png"), &display);
  let mut game = Game::new(&display, &font);

  let framerate = Duration::from_millis(16);
  let mut time = Instant::now();

  let mut closed = false;
  while !closed {
    if time.elapsed() >= framerate {
      let mut target = display.draw();
      target.clear_color(0.0, 0.0, 0.0, 1.0);
      game.draw(&mut target);
      target.finish().unwrap();
      time = Instant::now();
    }

    use glium::glutin::{VirtualKeyCode::*, WindowEvent::*};
    events_loop.poll_events(|e| match e {
      Event::WindowEvent { event: e, .. } => match e {
        CloseRequested => closed = true,
        KeyboardInput { input: e, .. } => {
          if e.state == ElementState::Pressed {
            match e.virtual_keycode.unwrap() {
              W | Up => game.rotate(),
              A | Left => game.move_tetromino(-1),
              D | Right => game.move_tetromino(1),
              S | Down => game.soft_drop(),
              Space => game.hard_drop(),
              R => game.reset(),
              _ => (),
            }
          }
        }
        _ => (),
      },
      _ => (),
    });
  }
}
